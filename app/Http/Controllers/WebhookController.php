<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    public function input(Request $request)
    {
        $answerId       = $request->input('id');
        $nextQuestionId = $request->input('next_question_id');

        if(!isset($nextQuestionId)){
            $answer         = new Answer();
            $answer         = $answer->find($answerId);

            if($answer->question()){
                return response()->json([
                    'success' => false,
                    'message' => 'question not found',
                ]);
            }

            $nextQuestionId = ($answer->question()->id + 1);
        }

        $question  = Question::find($nextQuestionId);

        return  response()->json([
            'success'  => true,
            'question' => $question
        ]);
    }
}
